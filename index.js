let numberName = '';
const tenName = ["", "ten", "twenty", "thirty", "forty", "fifty",
    "sixty", "seventy", "eighty", "ninety"];
const name = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen"];
function printNumbers(num) {
    for(let i = 1; i <= num; i ++){
        if(i < 100){
            numberName += getLow(i) + ", ";
        }
        else if(i < 1000){
            numberName += getHun(i) + getLow(i) + ", ";
        }
        else if(i < 10000){
            numberName += getThou(i) + getHun(i) + getLow(i) + ", ";
        }
        console.log(i);
        console.log(num);
    }
    numberName = numberName.split('');
    numberName[numberName.length - 2] = " ";
    numberName = numberName.join('');
    return numberName;
}
function getLow(num) {
    num = num % 100;
    if(num < 16)
        return name[num];
    else if(num < 20)
        return name[num % 10] + "teen";
    else
        return tenName[Math.floor(num/10) % 10] + " " + name[num % 10];
}
function getHun(num) {
    num = Math.floor(num/100 % 10);
    if(num > 0)
        return name[num] + " hundred ";
    return '';
}
function getThou(num) {
    if(num > 999)
        return name[Math.floor(num/1000)] + " thousand ";
}
